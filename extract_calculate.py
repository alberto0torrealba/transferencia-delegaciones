
import pypyodbc
from beem import Hive
from beem.account import Account
from beem.amount import Amount
from datetime import datetime, timedelta
import os
from dotenv import load_dotenv


# Cargar las variables de entorno desde el archivo .env
load_dotenv('.env')

# Obtener los valores de las variables
hived_nodes = os.getenv("HIVED_NODES").split(',')
username = os.getenv("USERNAME")
key = os.getenv("KEY")

# Objeto Hive para interactuar con la blockchain de Hive
hive = Hive(node=hived_nodes)

# Crea una instancia de la clase Account para la cuenta 'visualblock'
account = Account(username, blockchain_instance=hive)

# Función que se conecta a la base de datos de HiveSQL


def connect_to_database():
    server = os.getenv("DB_SERVER")
    database = os.getenv("DB_NAME")
    username_db = os.getenv("DB_USERNAME")
    password = os.getenv("DB_PASSWORD")
    connection_string = f"Driver={{ODBC Driver 17 for SQL Server}};Server={server};Database={database};uid={username_db};pwd={password}"
    connection = pypyodbc.connect(connection_string)
    return connection


def get_delegators_info():
    # Conexión a la base de datos de HiveSQL
    connection = connect_to_database()
    cursor = connection.cursor()
    SQLCommand = '''
    WITH cte AS (
      SELECT delegator, vesting_shares, timestamp,
             ROW_NUMBER() OVER (PARTITION BY delegator ORDER BY timestamp DESC) AS rn
      FROM TxDelegateVestingShares
      WHERE delegatee = 'visualblock'
    )
      SELECT delegator, vesting_shares, timestamp 
      FROM cte
      ORDER BY delegator, timestamp DESC
    '''

    # Ejecución de la consulta SQL y obtención del resultado
    cursor.execute(SQLCommand)
    result = cursor.fetchall()
    connection.close()

    # Diccionario que almacena la información de cada delegador
    delegators = {}
    for row in result:
        delegator = row[0]
        vesting_shares = float(row[1])
        # Conversión de VESTS a Hive Power
        hive_power_shares = hive.vests_to_hp(vesting_shares)
        hive_power_shares = float("{:.3f}".format(hive_power_shares))
        timestamp = row[2]
        if delegator in delegators:
            delegators[delegator].append(
                {'hive_power_shares': hive_power_shares, 'timestamp': timestamp})
        else:
            delegators[delegator] = [
                {'hive_power_shares': hive_power_shares, 'timestamp': timestamp}]

    return delegators

# Función que elimina a los delegadores que tienen una delegación actual de 0 HP


def remove_zero_delegators(delegators):
    for delegator in list(delegators):
        last_delegation = delegators[delegator][0]['hive_power_shares']
        if last_delegation == 0:
            del delegators[delegator]

# Función que obtiene la última delegación realizada por un delegador con antiguedad superior a 4 días


def get_last_delegation(delegator):
    for d in delegator:
        if datetime.utcnow() - d['timestamp'] > timedelta(days=4):
            return d
    return None

# Función que obtiene la información de los delegadores que han delegado HP a 'visualblock'
# y han sido aceptados para recibir delegación


def get_delegators_ok(delegators):
    delegators_list = []
    for delegator in delegators:
        last_delegation = get_last_delegation(delegators[delegator])
        if last_delegation:
            delegators_list.append(
                (delegator, last_delegation['hive_power_shares'], last_delegation['timestamp']))

    delegators_list = sorted(delegators_list, key=lambda x: x[1], reverse=True)
    return delegators_list

# Función que obtiene el HP total que han delegado a 'visualblock'


def get_total_hive_power_shares(delegators_list):
    total_hive_power_shares = 0
    for delegator_info in delegators_list:
        hive_power_shares = delegator_info[1]
        total_hive_power_shares += hive_power_shares
    return total_hive_power_shares

# Función que obtiene las recompensas de curación en los últimos 7 días


def get_curation_rewards_last_week():
    stop = datetime.utcnow() - timedelta(days=7)
    reward_vests = Amount("0 VESTS")
    for reward in account.history_reverse(stop=stop, only_ops=["curation_reward"]):
        reward_vests += Amount(reward["reward"])
    curation_rewards_last_week = hive.vests_to_hp(reward_vests)
    return curation_rewards_last_week

# Función que obtiene la cantidad total de HP de la cuenta "visualblock"


def get_total_hp_visualblock():
    total_hp_visualblock = account.get_steem_power(onlyOwnSP=True)
    return total_hp_visualblock

# Función que obtiene la cantidad total de HP de todas las cuentas en la cadena de bloques


def get_total_hp():
    total_hp = account.get_steem_power(onlyOwnSP=False)
    return total_hp

# Función que obtiene la cantidad total de curación efectiva en los últimos 7 días


def get_curation_rewards_last_week_effective(curation_rewards_last_week, total_hp_visualblock, total_hp):
    percentage = total_hp_visualblock / total_hp
    curation_rewards_last_week_effective = curation_rewards_last_week - \
        curation_rewards_last_week * percentage
    return curation_rewards_last_week_effective

# Función que devuelve una lista con el porcentaje aportado por cada delegador


def get_delegator_percentages(delegators_list, total_hive_power_shares):
    delegator_percentages = []
    for delegator_info in delegators_list:
        delegator = delegator_info[0]
        hive_power_shares = delegator_info[1]
        percentage = (hive_power_shares / total_hive_power_shares) * 100.00
        percentage_formatted = float("{:.3f}".format(percentage))
        delegator_percentages.append((delegator, percentage_formatted))
    return delegator_percentages

# Función que obtiene la cantidad de HIVE a transferir a cada delegador


def get_delegator_hive_transfer(delegator_percentages, curation_rewards_last_week_effective):
    delegator_hive_transfer = []
    for delegator_info in delegator_percentages:
        delegator = delegator_info[0]
        percentage = delegator_info[1]
        hive_transfer = (curation_rewards_last_week_effective) * \
            (percentage / 100)
        hive_transfer_formatted = float("{:.3f}".format(hive_transfer))
        delegator_hive_transfer.append((delegator, hive_transfer_formatted))
    return delegator_hive_transfer

# Función principal que calcula las recompensas


def calculate_rewards():
    delegators = get_delegators_info()
    remove_zero_delegators(delegators)
    delegators_list = get_delegators_ok(delegators)
    total_hive_power_shares = get_total_hive_power_shares(delegators_list)
    curation_rewards_last_week = get_curation_rewards_last_week()
    total_hp_visualblock = get_total_hp_visualblock()
    total_hp = get_total_hp()
    curation_rewards_last_week_effective = get_curation_rewards_last_week_effective(
        curation_rewards_last_week, total_hp_visualblock, total_hp)
    delegator_percentages = get_delegator_percentages(
        delegators_list, total_hive_power_shares)
    delegator_hive_transfer = get_delegator_hive_transfer(
        delegator_percentages, curation_rewards_last_week_effective)

    return (
        delegator_hive_transfer,
        total_hp,
        total_hp_visualblock,
        curation_rewards_last_week,
        curation_rewards_last_week_effective
    )
