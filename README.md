# Transferencias de Delegaciones

Este proyecto te permite calcular y realizar transferencias de recompensas de curación semanales a los delegadores de una cuenta en la cadena de bloques de Hive. Utiliza la biblioteca Beem para interactuar con la blockchain de Hive y la biblioteca pypyodbc para conectarse a la base de datos HiveSQL y obtener información sobre los delegadores.

## Pasos de preparación del proyecto

Sigue estos pasos para preparar y ejecutar el proyecto:

1. Clona el repositorio en tu máquina local:

### Clonar con SSH:

```
git clone git@gitlab.com:alberto0torrealba/transferencia-delegaciones.git
```

### Clonar con HTTPS:

```
git clone https://gitlab.com/alberto0torrealba/transferencia-delegaciones.git
```
2. Entra a la carpeta del proyecto:

```
cd transferencia-delegaciones
```

3. Crea un entorno virtual para el proyecto:

```
python -m venv venv

ó

python3 -m venv venv
```

4. Activa el entorno virtual:

- En Windows:

```
venv\Scripts\activate
```

- En Linux o macOS:

```
source venv/bin/activate
```

5. Instala las dependencias del proyecto desde el archivo `requirements.txt`:

```
pip install -r requirements.txt
```

6. Renombra el archivo `.env-example` a `.env` y configura las variables de entorno según tus necesidades. Asegúrate de proporcionar los valores correctos para las variables relacionadas con la conexión a la base de datos HiveSQL, y datos de la cuenta hive que realizará las trasferencias.

7. Instalar el controlador ODBC Driver 17 for SQL Server en Windows y Linux:

Windows:

- Descarga el instalador: Ve al sitio web oficial de Microsoft y descarga el controlador ODBC Driver 17 for SQL Server compatible con tu versión de Windows.

- Ejecuta el instalador: Una vez descargado el archivo de instalación, ejecútalo y sigue las instrucciones del asistente de instalación. Asegúrate de seleccionar las opciones necesarias y aceptar los términos de licencia.

Lunux:

- Agrega el repositorio de Microsoft: En sistemas basados en Debian/Ubuntu, ejecuta los siguientes comandos para agregar el repositorio de Microsoft:

```
sudo su
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/prod.list > /etc/apt/sources.list.d/mssql-release.list
exit
```

- Actualiza el sistema e instala el controlador: Ejecuta los siguientes comandos para actualizar el sistema y instalar el controlador ODBC:

```
sudo apt-get update
sudo apt-get install -y msodbcsql17
```

## Uso del proyecto

Una vez que hayas configurado el entorno y las variables de entorno, puedes ejecutar el proyecto. El archivo `main.py` que realiza el cálculo de las recompensas de curación y genera una lista de transferencias de Hive a realizar a los delegadores.Recuerda descomentar las líneas que hacen referencia al archivo
transfer.py y hacer pruebas antes de ejecutas la aplicación.

Puedes ejecutar el proyecto de la siguiente manera:

```
python main.py
```

El resultado será una lista de transferencias de Hive que se deben realizar a los delegadores. Estas transferencias se calculan en función de las recompensas de curación generadas y la cantidad de Hive Power delegado por cada delegador.

## Contribuciones

Las contribuciones son bienvenidas. Si encuentras errores, mejoras o nuevas características que se puedan agregar al proyecto, no dudes en enviar una solicitud de extracción.
También puedes dejar un comentario en mi blog, [visitar aquí.](https://ecency.com/hive-154226/@alberto0607/script-en-python-calcular-y)

## Licencia

Este proyecto se ofrece bajo los principios del software libre y el código abierto, lo que significa que puedes utilizarlo, modificarlo y distribuirlo de acuerdo con las libertades que ofrece este tipo de licencias. Se proporciona sin garantía alguna, y la responsabilidad recae en el usuario que lo utilice. Te invitamos a aprovechar al máximo las ventajas del software libre y a contribuir a su comunidad
