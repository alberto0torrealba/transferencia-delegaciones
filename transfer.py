from beem import Hive
from beem.account import Account
import time
import os
from dotenv import load_dotenv


def transfer_delegators(delegator_hive_transfer):
    # Cargar las variables de entorno desde el archivo .env
    load_dotenv('.env')

    # Obtener los valores de las variables
    hived_nodes = os.getenv("HIVED_NODES").split(',')
    username = os.getenv("USERNAME")
    key = os.getenv("KEY")
    memo = os.getenv("MEMO")

    # Obtener instancia de Hive
    hive = Hive(node=hived_nodes, keys=[key])

    # Función que obtiene el saldo de una cuenta.
    def get_account_balance(account_name, hive):
        account = Account(
            account_name, blockchain_instance=hive)
        total_base = account['balance']
        base_symbol = total_base.symbol
        return total_base, base_symbol

    # Función que transfiere una cantidad de una moneda a un destinatario.
    def transfer_funds(sender_name, transfer_list, symbol, memo, hive):
        print(sender_name)
        print(transfer_list)
        print(symbol)
        print(memo)
        print(hive)
        sender = Account(sender_name, blockchain_instance=hive)
        for recipient_name, amount in transfer_list:
            recipient = Account(
                recipient_name, blockchain_instance=hive)
            sender.transfer(recipient.name, amount, symbol, memo=memo)
            time.sleep(5)
            print(f"Usuario: {recipient_name}, Monto: {amount}, Memo: {memo}")

    # Obtener saldo de la cuenta de pago
    total_base, base_symbol = get_account_balance(username, hive)
    print(f"Total balance: {total_base}")
    print(f"Symbol: {base_symbol}")

    # Transferir fondos a una lista de destinatarios
    print(delegator_hive_transfer)
    transfer_list = delegator_hive_transfer
    # Lista para pruebas. Edita y descomenta esta línea para que realices tus pruebas.
    # transfer_list = transfer_list = [('visualblock', '0.001'), ('alberto0607', '0.001')]
    symbol = base_symbol
    transfer_funds(username, transfer_list, symbol, memo, hive)
