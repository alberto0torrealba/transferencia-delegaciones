from extract_calculate import (
    get_delegators_info,
    remove_zero_delegators,
    get_delegators_ok,
    get_total_hive_power_shares,
    get_total_hp_visualblock,
    get_total_hp,
    get_curation_rewards_last_week,
    get_curation_rewards_last_week_effective,
    get_delegator_percentages,
    get_delegator_hive_transfer,
)
# from transfer import transfer_delegators
from markdown_table import generate_markdown_table, table_list


def main():
    # Obtener información de los delegadores
    delegators = get_delegators_info()
    remove_zero_delegators(delegators)
    delegators_list = get_delegators_ok(delegators)

    # Calcular los valores necesarios
    total_hivepower_delegators = get_total_hive_power_shares(delegators_list)
    total_hp_visualblock = get_total_hp_visualblock()
    total_hp = get_total_hp()
    curation_rewards_last_week = get_curation_rewards_last_week()
    curation_rewards_last_week_effective = get_curation_rewards_last_week_effective(
        curation_rewards_last_week, total_hp_visualblock, total_hp
    )

    # Calcular porcentajes y transferencias
    total_hive_power_shares = get_total_hive_power_shares(delegators_list)
    delegator_percentages = get_delegator_percentages(
        delegators_list, total_hive_power_shares)
    delegator_hive_transfer = get_delegator_hive_transfer(
        delegator_percentages, curation_rewards_last_week_effective)

    # Mostrar los resultados
    print(f"Total de HP de delegadores: {total_hivepower_delegators:.2f} HP")
    print(f"Total de HP de Visualblock: {total_hp_visualblock:.2f} HP")
    print(f"Total de HP de Visualblock + delegadores: {total_hp:.2f} HP")
    print(
        f"Recompensas de curación de los últimos 7 días: {curation_rewards_last_week:.2f} HP")
    print(
        f"Recompensas de curación de los últimos 7 días Efectivo: {curation_rewards_last_week_effective:.2f} HP")
    print("###############################")
    print(delegators_list)
    print("###############################")
    print(delegator_percentages)
    print("###############################")
    print(delegator_hive_transfer)

    # Generar tabla Markdown
    markdown_table = generate_markdown_table(
        delegators_list, delegator_hive_transfer, delegator_percentages)
    print(markdown_table)

    # Generar y guardar la tabla
    table_list(markdown_table)

    # Transferir HP a los delegadores

    # transfer_delegators(delegator_hive_transfer)


if __name__ == '__main__':
    main()
