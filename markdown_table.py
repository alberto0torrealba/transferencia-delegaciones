# Función que obtiene la nueva lista con todos los datos

def generate_markdown_table(hive_power, transfer_list, percentage_list):
    new_list = []
    # Itera sobre las tres listas de entrada simultáneamente
    for (usuario1, datos1, _), (usuario2, datos2), (usuario3, datos3) in zip(hive_power, transfer_list, percentage_list):
        # Verifica si los usuarios en las tres listas coinciden
        if usuario1 == usuario2 == usuario3:
            new_list.append((usuario1, datos1, datos2, datos3))
    return new_list

# Función que crea la tabla markdown


def table_list(new_list):
    new_route = 'table.txt'
    save_file = open(new_route, 'r+')

    table_header = '''| # | USUARIOS | PODER HIVE | MONTO DE TRANSFERENCIA | PORCENTAJE |
    | ----- | ----- | ----- | ----- | ----- |\n'''

    # Escribe el encabezado de la tabla en el archivo
    save_file.write(table_header)

    # Variables para almacenar la suma de cada columna
    sum_hive_power = 0
    sum_transfer_amount = 0
    sum_percentage = 0

    for i, (usuario, datos1, datos2, datos3) in enumerate(new_list):
        poder_hive = datos1
        monto_transferencia = datos2
        porcentaje = datos3

        # Suma los valores de cada columna
        sum_hive_power += float(poder_hive)
        sum_transfer_amount += float(monto_transferencia)
        sum_percentage += float(porcentaje)

        count = i + 1
        table_data = f'| {count} | @{usuario} | {poder_hive} | {monto_transferencia} | {porcentaje} |\n'

        save_file.write(table_data)

    # Construye una nueva fila con las sumas de cada columna
    sum_row = f'|   | SUMA  | {sum_hive_power:.3f} | {sum_transfer_amount:.3f} | {sum_percentage:.3f} |\n'
    save_file.write(sum_row)

    save_file.close()
